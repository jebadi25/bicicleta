var Passport = require('../../config/passport');
var express = require('express');
var router = express.Router();
var Usuario = require('../../models/usuario');
const usuario = require('../../models/usuario');

//Session endpoints
router.post('/login', (req, res, next) => {
  Passport.authenticate('local', (err, usuario, info) => {
    console.log('err, usuario, info', err, usuario, info);
    console.log('ENTREEEEE');
    if (err) return next(err);
    if (!usuario) return res.render('session/login', { info });
    req.login(usuario, (err) => {
      if (err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);
});

router.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/');
});

router.post('/forgotPassword', (req, res, next) => {
  Usuario.findOne({ email: req.body.email }, (err, usuario) => {
    if (!usuario)
      return res.render('session/forgotPassword', {
        info: { message: 'No existe un usuario con ese email' },
      });

    usuario.resetpassword((err) => {
      if (err) return next(err);
      console.log('session/forgotPasswordMessage');
    });

    res.render('session/forgotPasswordMessage');
  });
});

router.post('/resetPassword', (req, res, next) => {
  if (req.body.password != req.body.confirm_password) {
    res.render('session/resetPassword', {
      errors: {
        confirm_password: { message: 'No coincide con el password ingresado.' },
      },
      usuario: new usuario({ email: req.body.email }),
    });
    return;
  }

  Usuario.findOne({ email: req.body.email }, (err, usuario) => {
    usuario.password = req.body.password;
    usuario.save((err) => {
      if (err) {
        res.render('session/resetPassword', {
          errors: err.errors,
          usuario: new Usuario({ email: req.body.email }),
        });
      } else {
        res.redirect('/session/login');
      }
    });
  });
});

module.exports = router;