var express = require("express");
var router = express.Router();
var usuarioController = require("../../controllers/api/usuarioControllerAPI");

router.get("/", usuarioController.usuarios_list);
router.post("/create", usuarioController.usuario_create);
router.put("/update", usuarioController.usuario_update);
router.delete("/delete", usuarioController.usuario_delete);
router.post("/reservar", usuarioController.usuario_reservar);

module.exports = router;