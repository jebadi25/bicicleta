var express = require('express');
var router = express.Router();
var authController = require('../../controllers/api/authControllerAPI');
const Passport = require('passport');

router.post('/authenticate', authController.authenticate);
router.post('/forgotPassword', authController.forgotPassword);

/*router.post(
    '/facebook_token',
    Passport.authenticate('facebook-token'),
    AuthController.authFacebookToken
  );

*/
module.exports = router;