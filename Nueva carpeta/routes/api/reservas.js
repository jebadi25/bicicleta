var express = require("express");
var router = express.Router();
var reservasController = require("../../controllers/api/reservasControllerAPI");

router.get("/", reservasController.reservas_list);
router.delete("/removeAll", reservasController.remove_all);

module.exports = router;

