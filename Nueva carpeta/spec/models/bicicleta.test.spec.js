var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function() {
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.disconnect();
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database! BICICLETA');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crear instancia de Bicicleta', (done) => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);

            done();
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('Comienza vacía', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('Agrega una bici', (done) => {
            var aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
            Bicicleta.add(aBici, function(err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Devolver bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });

                Bicicleta.add(aBici, function(err, newBici) {
                    if (err) console.log(err);
                    var aBici2 = new Bicicleta({ code: 2, color: "roja", modelo: "urbana" });

                    Bicicleta.add(aBici2, function(err, newBici) {
                        if (err) console.log(err);

                        Bicicleta.findByCode(1, function(error, targetBici) {
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            done();
                        });
                    });
                });
            });
        });
    });
});






/*var Bicicleta = require('../../models/bicicleta');

beforeEach( ()=> {
    Bicicleta.allBicis = [];
});

describe('Bicicleta.allBicis', () =>{
    it('comienza vacia', ()=>{
        exports(Bicicleta.allBicis.length).toBe(0);
    });
});


describe('Bicicleta.add', () =>{
    it('agregamos una', ()=>{
        exports(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
        Bicicleta.add(a);

        exports(Bicicleta.allBicis.length).toBe(1);
        exports(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById', () =>{
    it('bucamos una bici', ()=>{
        exports(Bicicleta.allBicis.length).toBe(0);

        var aBici1 = new Bicicleta(1, 'rojo', 'urbana');
        var aBici2 = new Bicicleta(2, 'verde', 'montaña');
        Bicicleta.add(aBici1);
        Bicicleta.add(aBici2);

        var targetBici = Bicicleta.findById(1);

        exports(targetBici.id).toBe(1);
        exports(targetBici.color).toBe(aBici.color);
        exports(targetBici.modelo).toBe(aBici.modelo);
    });
});*/

