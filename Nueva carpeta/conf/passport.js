const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const FacebookTokenStrategy = require('passport-facebook-token');
const Usuario = require('../models/usuario');

//auntenticacion con facebook
/*
passport.use(
    new FacebookTokenStrategy(
      {
        clientID: process.env.FACEBOOK_ID,
        clientSecret: process.env.FACEBOOK_SECRET,
      },
      (accesToken, refreshToken, profile, done) => {
        try {
          Usuario.findOneOrCreateByFacebook(profile, (err, res) => {
            if (err) console.log('err', err);
            return done(err, user);
          });
        } catch (err2) {
          console.log('err2', err2);
          return done(err2, null);
        }
      }
    )
  );


//autenticacion con google

passport.use(
    new LocalStrategy((username, password, done) => {
      console.log('username, password', username, password);
      Usuario.findOne({ email: username }, (err, usuario) => {
        console.log('usuario', usuario);
        if (err) {
          return done(err);
        }
        if (!usuario) {
          return done(null, false, {
            message: 'Email no existente o incorrecto.',
          });
        }
        if (!usuario.validatePassword(password)) {
          return done(null, false, { message: 'Contrasena incorrecta.' });
        }
        return done(null, usuario);
      });
    })
  );
*/

//autenticacion local  

passport.use(new LocalStrategy (
    function(email, password, done) {
        Usuario.findOne({ email: email }, function (err, usuario) {
            if (err) return done(err);
            if (!usuario) return done(null, false, { message: 'Email no existente o incorrecto.' });
            if (!usuario.validPassword(password)) return done(null, false, { message: 'Password incorrecto' });

            return done(null, usuario);
        });
    }
));

passport.serializeUser(function(usuario, cb) {
    cb(null, usuario.id);
});

passport.deserializeUser(function(id, cb) {
    Usuario.findById(id, function(err, usuario) {
        cb(err, usuario);
    });
});

module.exports = passport;