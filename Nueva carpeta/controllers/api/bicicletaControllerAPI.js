const Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function (req, res) {
    Bicicleta.find({}, (err, bicicletas) => {
        if (err) console.log(err);

        res.status(200).json({
            bicicletas: bicicletas
        });
    });
};

exports.bicicleta_create = function (req, res) {
    var bici = new Bicicleta({ code: req.body.code, color: req.body.color, modelo: req.body.modelo });
    bici.ubicacion = [req.body.lat, req.body.lng];

    bici.save(function(err) {
        if (err) console.log(err);

        res.status(201).json(bici);
    });
};

exports.bicicleta_update = function (req, res) {
    Bicicleta.findByCode(req.body.code, (err, targetBici) => {
        if (err) console.log(err);

        targetBici.code = req.body.code
        targetBici.color = req.body.color;
        targetBici.modelo = req.body.modelo;
        targetBici.ubicacion = [req.body.lat, req.body.lng];
        targetBici.save();

        res.status(203).json({
            targetBici: targetBici
        });
    });
};

exports.bicicleta_delete = function (req, res) {
    Bicicleta.removeByCode(req.body.code, (err, result) => {
        if (err) console.log(err);

        res.status(204).send(result);
    });
};